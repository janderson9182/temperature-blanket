# Temperature Blanket
This is a quick project idea that I had to automate the repetitive task of 
getting weather data to make a temperature blanket.

## What is a Temperature Blanket
A temperature blanket is a blanket (no surprise there) that is knitted,
crocheted etc throughout the year with each strand having a colour representing
the temperature for each day. If you do an image search for "Temperature Blanket" or "Tempestries"
you'll see some great examples.

[!Temperature Blanket Photo](https://en.wikipedia.org/wiki/File:Tempestries_for_Utqiagvik,_AK_and_Death_Valley,_CA.jpg)

For example, I live in Northern Ireland and use Celsius for temperature, so less than
or equal to 0 degrees could be "Pistachio", 1-3 degrees could be "Meadow". See Colour.java.

Temperature blanket Ideas:
* Each day of current year
* Each day of the year someone was born
* Each day of a pregnancy term (then you've a baby blanket at the end)

## Getting started with the repo
* You'll need to update Temperature.java with the latitude & longitude of your desired place.
* You'll need to generate an api key on http://openweathermap.org and make a paid account (^ note below)
* App.java has the main method you can run.
 
> ^ It's unlikely that you'll exceed the free limit but a credit/debit card is required to get access
> to that part of the api. If I know you I can lend you my key. It's the ""One Call by Call" subscription plan".
> I tried the non-paid one and was getting a 401 unauthorized with my api key. If you can get it working, feel free
> to fork and create a merge request. I linked api docs in the Temperature.java file.

## Running from command line
```bash
javac *.java
java App
```

## Contributing
* Feel free to fork and make merge requests
* I have created some issues with feature requests.
* I don't want to bring in third party libraries, this project was an exploration into using native java
* I love unit testing and feel free to add something basic if you want even using `assert` statements but I have not done that here.
* I think the idea of making a quick code change to suit your own needs, compile and run is fine for this project e.g. if you want your own colour scheme and to say to use Fahrenheit you can make those code changes yourself when you're using it. The project isn't so big that I think this would be hard to do (your mileage [kilometer-age] may vary) 
* Please feel free to create issues if you are struggling to get this to work.
* Remember, scaling and production-ready performance is NOT an issue here
* I like code style that lends itself to readability E.g `Colour.fromTemperature(temperature)`, `CsvFile.append(row)` or `Temperature.on(date)` but that isn't always possible.
