/**
 * Class Row represents one row in the CSV file.
 * It also has methods for translating to and from the csv lines
 */
public class Row {
    private final String date;
    private final String colour;
    private final int temperature;

    public Row(final String date, final String colour, final int temperature) {
        this.date = date;
        this.colour = colour;
        this.temperature = temperature;
    }

    public static Row fromCsvLine(final String csvLine) {
        String[] parts = csvLine.split(",");
        if (parts.length != 3) {
            throw new IllegalArgumentException("Invalid CSV line [%s]".formatted(csvLine));
        }

        final String date = parts[0];
        final String colour = parts[1];
        final int temperature = Integer.parseInt(parts[2]);
        return new Row(date, colour, temperature);
    }

    public String toCsvLine() {
        return "%s,%s,%d".formatted(date, colour, temperature);
    }

    @Override
    public String toString() {
        return toCsvLine();
    }
}
