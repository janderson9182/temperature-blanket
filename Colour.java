import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

/*
 * Class to represent the colour to temperature rules
 */
public class Colour {
    private static final Map<Integer, String> COLOUR_FOR_TEMPERATURE;

    static {
        COLOUR_FOR_TEMPERATURE = new HashMap<>();
        IntStream.rangeClosed(1, 3).forEach(number -> COLOUR_FOR_TEMPERATURE.put(number, "Meadow"));
        IntStream.rangeClosed(4, 6).forEach(number -> COLOUR_FOR_TEMPERATURE.put(number, "Cypress"));
        IntStream.rangeClosed(7, 9).forEach(number -> COLOUR_FOR_TEMPERATURE.put(number, "Bottle"));
        IntStream.rangeClosed(10, 12).forEach(number -> COLOUR_FOR_TEMPERATURE.put(number, "Khaki"));
        IntStream.rangeClosed(13, 15).forEach(number -> COLOUR_FOR_TEMPERATURE.put(number, "Camel"));
        IntStream.rangeClosed(16, 18).forEach(number -> COLOUR_FOR_TEMPERATURE.put(number, "Gold"));
        IntStream.rangeClosed(19, 21).forEach(number -> COLOUR_FOR_TEMPERATURE.put(number, "Copper"));
        IntStream.rangeClosed(22, 24).forEach(number -> COLOUR_FOR_TEMPERATURE.put(number, "Gingerbread"));
        IntStream.rangeClosed(25, 27).forEach(number -> COLOUR_FOR_TEMPERATURE.put(number, "Mocha"));
        IntStream.rangeClosed(28, 30).forEach(number -> COLOUR_FOR_TEMPERATURE.put(number, "Walnut"));
    }

    public static String fromTemperature(final int temperature) {
        if (temperature <= 0) {
            return "Pistachio";
        } else if (temperature >= 31) {
            return "Dark Brown";
        } else {
            return COLOUR_FOR_TEMPERATURE.get(temperature);
        }
    }
}
