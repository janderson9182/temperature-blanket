import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;

class App {
    public static void main(String[] args) {
        try {
            run();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void run() throws IOException, InterruptedException {
        List<LocalDateTime> datesInCurrentYear = getDatesSoFarThisYear();
        List<Row> rows = new ArrayList<>(datesInCurrentYear.size());

        for (LocalDateTime date : datesInCurrentYear) {
            int temperature = Temperature.on(date);
            String colour = Colour.fromTemperature(temperature);

            Row row = new Row(date.toString(), colour, temperature);
            System.out.println(row);
            rows.add(row);
        }

        CsvFile.append(rows);
    }

    private static List<LocalDateTime> getDatesSoFarThisYear() {
        int currentYear = LocalDateTime.now().get(ChronoField.YEAR);
        LocalDateTime current = LocalDate.of(currentYear, Month.JANUARY, 1).atTime(12, 0);
        return getDatesBetweenInclusive(current, LocalDateTime.now());
    }

    /*
     * Use this instead of getDatesSoFarThisYear() if you want a different range.
     */
    private static List<LocalDateTime> getDatesBetweenInclusive(final LocalDateTime from, final LocalDateTime to) {
        final List<LocalDateTime> datesSoFarThisYear = new ArrayList<>();

        datesSoFarThisYear.add(from);

        LocalDateTime current = from;
        while (to.getDayOfYear() > current.getDayOfYear()) {
            current = current.plusDays(1);
            datesSoFarThisYear.add(current);
        }

        return datesSoFarThisYear;
    }
}
