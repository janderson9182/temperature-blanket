import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Utility class to abstract the API call to get the temperature in celsius
 */
public class Temperature {
    public static int on(LocalDateTime date) throws IOException, InterruptedException {
        // API docs: https://openweathermap.org/api/one-call-3#history
        long dateEpoch = date.atZone(ZoneId.systemDefault()).toEpochSecond();
        String start = Long.toString(dateEpoch);
        String lat = "CHANGE_ME";// e.g. "54.59682"
        String lon = "CHANGE_ME";// e.g. "-5.92541"
        String apiKey = "CHANGE_ME";

        validateParamsChanged(lat, lon, apiKey);

        String url = "https://api.openweathermap.org/data/3.0/onecall/timemachine?lat=%s&lon=%s&dt=%s&appid=%s&units=metric".formatted(lat, lon, start, apiKey);

        HttpClient httpClient = HttpClient.newBuilder().build();
        HttpResponse<String> result = httpClient.send(
                HttpRequest.newBuilder(URI.create(url)).build(),
                HttpResponse.BodyHandlers.ofString()
        );

        String body = result.body();
        return getTemperatureFromJson(body);
    }

    private static void validateParamsChanged(String... params) {
        for (String param : params) {
            if ("CHANGE_ME".equals(param)) {
                throw new IllegalArgumentException("Please change the parameters in Temperature.java");
            }
        }
    }

    private static int getTemperatureFromJson(String responseJson) {
        // sometimes there's no decimal point
        String regex = "\"temp\":(-?\\d*\\.?\\d*)";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(responseJson);
        if (matcher.find()) {
            double resultDouble = Double.parseDouble(matcher.group(1));
            return (int) resultDouble;
        }

        // if this is the case, you need to do some debugging on the regex
        System.out.println("No temperature match found, returning zero " + responseJson);
        return 0;
    }
}
