import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/*
 * Utility class for working with the CSV file
 */
public class CsvFile {

    public static final String FILE_NAME = "colour-temps.csv";

    public static void append(final List<Row> rows) {
        try {
            File file = new File(FILE_NAME);
            file.createNewFile();

            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true))) {
                for (Row row : rows) {
                    bufferedWriter.append(row.toCsvLine());
                    bufferedWriter.newLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Row> read() {
        try {
            File file = new File(FILE_NAME);
            file.createNewFile();

            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
                return bufferedReader
                        .lines()
                        .map(Row::fromCsvLine)
                        .collect(Collectors.toList());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Problems reading file. Returning empty list.");
        return List.of();
    }
}
